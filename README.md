## Get Site Copy

## Bot

Bot will be available at [https://t.me/GetSiteCopyBot](https://t.me/GetSiteCopyBot) after deployment.


## Settings
You have to create a `.env` file in the root of your project (see `.env.template`):

**First of all**, obtain `.google-config.json` file and add it to the root of the project.

```dotenv
# General Settings
PROJECT_NAME=getcopy

# See https://fastapi.tiangolo.com/tutorial/cors/
BACKEND_CORS_ORIGINS=["http://localhost:8000", "https://localhost:8000", "http://localhost", "https://localhost"]

# Database Settings
POSTGRES_USER=USERNAME
POSTGRES_PASSWORD=PASSWORD
POSTGRES_SERVER=SERVER
POSTGRES_DB=DB_NAME

# Telegram Webhook URL
WEBHOOK_URL=https://gscapp.roskomsvoboda.org/bot/

# Obtain token from @BotFather
TELEGRAM_BOT_API_TOKEN="TELEGRAM_BOT_TOKEN"

# Google Cloud Storage
GOOGLE_CLOUD_STORAGE_BUCKET=get_site_copy
```

## Development

You have to install all the dependencies first:

```bash
poetry install
```

Then you can run the development server:

```
poetry run uvicorn app.main:app 
```

## Assets

To add static data to Google storage, install [gsutil](https://cloud.google.com/storage/docs/gsutil) and use the command:


For production version: 
```
bash scripts/upload-assets-prod.sh
```

For developer version: 
```
bash scripts/upload-assets-dev.sh
```

## Production

Please, see [Deployment](https://fastapi.tiangolo.com/deployment/).