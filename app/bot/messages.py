START_MESSAGE = f"""
Привет! 👋

Я бот, который поможет вам сгенерировать рабочую ссылку на новость или статью с сайта заблокированного СМИ.

Отправьте мне ссылку, которой хотите поделиться.
"""  # noqa: E501

HELP_MESSAGE = f"""
Данный бот создан для того, чтобы вы могли безпрепятственно делиться ссылками с заблокированных в России сайтов СМИ.

Чтобы получить рабочую ссылку на статью или новость, отправьте боту ссылку на статью с одного из сайтов заблокированного СМИ.

"""  # noqa: E501

FINAL_RESULT_MESSAGE = """
🔗 Копия статьи создана! Теперь вы можете делиться данной ссылкой.

Нажмите по ссылке ниже, чтобы скопировать ее:
"""

ADMIN_ID = [
    "5013051586",
]

BOT_COMMANDS = [
    "Рейтинг",
    "Статистика",
    "Отмена",
    "/cancel",
    "/help",
    "/start",
]

USER_COMMANDS = [
    "Отмена",
    "/cancel",
    "/help",
    "/start",
]

MEDIA_LIST = [
    "zona.media",
    "paperpaper.ru",
    "holod.media",
    "ovdinfo.org",
    "ovd.news",
    "baikal-journal.ru",
    "gubernia.media",
    "semnasem.org",
    "avtonom.org",
    "itsmycity.ru",
    "www.bbc.com",
    "skat.media",
    "cherta.media",
    "protokol.band",
    "echofm.online",
    #"newtimes.ru",
    #"meduza.io",
    #"www.svoboda.org",
    #"www.golosameriki.com",
    #"data.ovdinfo.org",
    #"vot-tak.tv",
    #"bbc.in",
    #"te-st.ru",
    #"rus.azathabar.com",
    #"news.doxajournal.ru",
    #"republic.ru",
    #"www.azadliq.org",
    #"www.azathabar.com",
    #"www.radiofarda.com",
    #"www.proekt.media",
    #"zasekin.ru",
]

MESSAGE_TEMPLATE = """
Отпишите после этого сообщения, с чем у вас возникли проблемы

(Если хотите отменить действие, нажмите "Отмена" или пропишите "/cancel")
"""

QUESTION_CHOICE = """
Здравствуйте!

Если у вас возник вопрос, нажмите на <b>"Задать вопрос"</b>.

Если же возникли проблемы с генерацией копии материала, то нажмите на <b>"Проблемы с ботом"</b>.

"""
