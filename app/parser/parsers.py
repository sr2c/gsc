import hashlib
import io
import typing as t

import aiohttp
import lxml.html
import requests
from bs4 import BeautifulSoup
from lxml.html import HTMLParser
from lxml.html.clean import Cleaner as HTMLCleaner

from app.core.storage import gcs
from app.parser.jinja import jinja_env
from app.core.config import settings
from .exceptions import UnsupportedWebsite
from .utils import extract_hostname, normalize

__all__ = ["create_parser_object"]

KeyFunction = t.Callable[[t.Any], t.Any]
XPathRule = dict[str, t.Any]
XPathRules = list[XPathRule]


class WebPage:
    def __init__(self, url: str, content: str) -> None:
        self.url = url
        self.soup = BeautifulSoup(content, "html.parser")

    def hostname(self) -> str:
        return extract_hostname(self.url)

    def filename(self):
        return f"{self.hostname()}/{self.sha1()[:8]}.html"

    def sha1(self) -> str:
        return hashlib.sha1(self.url.encode("utf-8")).hexdigest()

    def html(self, parser=None, encoding="utf-8") -> str:
        return lxml.html.fromstring(
            html=self.soup.prettify(encoding=encoding),
            parser=parser,
        )


class Parser:
    """Article content parser.

    All subclasses must implement the following attributes:

    title_xpath: XPath expression for the title of the article.
    article_xpath: XPath expression for the article content.
    timestamp_xpath: XPath expression for the article timestamp.
    remove_on_xpath_match: List of tuples of (tag, regex) for tags to exclude from article.

    See https://devhints.io/xpath for more info about xpath.
    """

    title_xpath: str | None = None
    tag_title_xpath: str | None = None
    article_xpath: str
    image_preview_xpath: str | None = None
    timestamp_xpath: str | None = None
    remove_on_xpath_match: XPathRules = []
    page_header_template: str | None = None
    page_footer_template: str | None = None
    google_analytics_tracking_id: str | None = None
    article_image_xpath: str | None = None
    description_xpath: str | None = None
    favicon_path: str | None = None
    custom_styles: list[str] = []
    custom_js: list[str] = []
    need_the_url: str | None = None
    styles_link: str | None = None
    encoding = "utf-8"
    bucket_name = settings.GOOGLE_CLOUD_STORAGE_BUCKET 

    # Default HTML Parser:
    html_parser = HTMLParser(
        recover=True,
        remove_comments=True,
        remove_blank_text=True,
    )
    # Default HTML Cleaner:
    html_cleaner = HTMLCleaner(
        forms=False,
        scripts=False,
        embedded=False,
        annoying_tags=False,
        remove_unknown_tags=False,
        safe_attrs_only=False,
    )

    def __init__(self, web_page: WebPage) -> None:
        self.page = web_page
        self.html = self.page.html(
            parser=self.html_parser,
            encoding=self.encoding,
        )

    def find(self, selector: str, key: KeyFunction | None = None) -> t.Any:
        """Finds the first element matching the selector."""

        element = self.html.xpath(selector)
        if len(element) < 1:
            raise ValueError(f"No element found: {selector}")

        element = element[0]

        if not key:
            return element
        return key(element)

    def get_og_image(self) -> t.Optional[str]:
        """Returns the image preview of the article."""
        try:
            preview = self.find(self.image_preview_xpath)
            return gcs.upload_image(preview)
        except BaseException:
            return None

    def get_article_image(self) -> t.Optional[str]:
        try:
            preview = self.find(self.article_image_xpath)
            return gcs.upload_image(preview)
        except BaseException:
            return None

    def get_timestamp(self) -> t.Union[str, None]:
        """Returns the timestamp of the article."""
        try:
            if not self.timestamp_xpath:
                return None

            result = self.find(self.timestamp_xpath, key=normalize)

            if result and "T" in result:
                return result.split("T")[0].strip()

            return None
        except:
            return None

    def get_title(self) -> str:
        """Returns the title of the article."""
        if self.title_xpath:
            return self.find(self.title_xpath, key=normalize)
        else:
            return None

    def get_tag_title_xpath(self) -> t.Union[str, None]:
        """Returns the title of the article."""
        if self.tag_title_xpath:
            title = self.find(self.tag_title_xpath)
            title = lxml.html.tostring(
                doc=title,
                pretty_print=True,
                encoding="unicode",
            )
            return normalize(self.html_cleaner.clean_html(title))

        if not self.tag_title_xpath:
            return None

    def get_description(self) -> t.Union[str, None]:
        """Returns the description of the article."""
        try:
            return self.find(self.description_xpath, key=normalize)
        except:
            return None

    def get_need_the_url(self):
        url = self.need_the_url
        if url:
            return True
        else:
            return None

    def get_styles_link(self):
        if self.styles_link:
            custom_link_styles = []
            domain = f"https://{self.page.hostname()}"
            links = self.find(self.styles_link)

            for link in links:
                rel = link.get("rel")
                href = link.get("href")
                style = requests.get(f"{domain}{href}")

                if "stylesheet" in rel:
                    link_tag = f'<style type="text/css">{style.text.strip()}</style>'
                    link_formatted = lxml.html.fromstring(link_tag)
                    custom_link_styles.append(link_formatted)

            return custom_link_styles
        if not self.styles_link:
            return None

    def _replace_video_content(self, content) -> None:
        pass

    def _replace_audio_content(self, content) -> None:
        pass

    def _upload_images_and_replace(self, content) -> None:
        images = content.xpath(".//img[@src]|.//img[@data-src]")

        ignored_attributes = (
            "height",
            "id",
            "sizes",
            "srcset",
            "styles",
            "width",
        )

        if images:
            for image in images:
                src = image.attrib.get("src")

                if not src:
                    data_src = image.attrib.get("data-src")
                    if data_src:
                        src = data_src

                for attr in ignored_attributes:
                    image.attrib.pop(attr, None)

                # If the image is base64 encoded, we don't need to upload it.
                if src and not src.startswith("data:"):
                    # If src is relative, we need to make it absolute.
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"
                    # Upload the image and replace the src attribute.
                    image.attrib.update({"src": gcs.upload_image(src)})

    def _remove_on_xpath_match(self, content) -> None:
        for rule in self.remove_on_xpath_match:
            xpath = rule.get("xpath", None)
            namespaces = None
            if "re:" in xpath:
                namespaces = {
                    "re": "http://exslt.org/regular-expressions",
                }
            for tag in content.xpath(xpath, namespaces=namespaces):
                tag.drop_tree()

    @staticmethod
    def _remove_tag_attributes(content) -> None:
        content.attrib.clear()
        for element in content:
            element.attrib.clear()

    def _upload_links_and_replace(self, content):
        links = content.xpath(".//a[@href]")

        for link in links:
            href = link.attrib.get("href")

            if href.startswith("/"):
                href = f"https://{self.page.hostname()}{href}"
                link.attrib.update({"href": href})

    def get_content(self) -> str:
        """Returns the content of the article."""
        content = self.find(self.article_xpath)
        self._upload_images_and_replace(content)
        self._remove_on_xpath_match(content)
        self._upload_links_and_replace(content)
        self._replace_video_content(content)
        self._replace_audio_content(content)
        content = lxml.html.tostring(
            doc=content,
            pretty_print=True,
            encoding="unicode",
        )
        return normalize(self.html_cleaner.clean_html(content))

    def get_context(self) -> dict:
        """Returns the Jinja-context of the article."""
        return {
            "title": self.get_title(),
            "tag_title": self.get_tag_title_xpath(),
            "content": self.get_content(),
            "source_url": self.page.url,
            "need_the_url": self.get_need_the_url(),
            "favicon": self.favicon_path,
            "datetime": self.get_timestamp(),
            "source_name": self.page.hostname(),
            "article_url": self.page.filename(),
            "description": self.get_description(),
            "article_image": self.get_article_image(),
            "og_image": self.get_og_image(),
            "page_header_template": self.page_header_template,
            "page_footer_template": self.page_footer_template,
            "custom_styles": self.custom_styles,
            "custom_js": self.custom_js,
            "styles_link": self.get_styles_link(),
            "google_analytics_tracking_id": self.google_analytics_tracking_id,
            "bucket_name": self.bucket_name,
        }

    def result(self) -> t.IO:
        """Returns the article as a file."""
        context = self.get_context()
        template = jinja_env.get_template("article-template.html")
        html = template.render(**context).encode("utf-8")
        return io.BytesIO(html)


class OpenGraphXPathMixin:
    title_xpath = '//meta[@property="og:title"]/@content'
    tag_title_xpath = None
    styles_link = None
    need_the_url = True
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    description_xpath = '//meta[@property="og:description"]/@content'
    page_footer_template = "footer/base_footer.html"


class ArticleParser(OpenGraphXPathMixin, Parser):
    pass


class MeduzaParser(ArticleParser):
    article_xpath = '//*[@class="GeneralMaterial-article"]'
    timestamp_xpath = "//time/text()"
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/meduza.html"
    favicon_path = "assets/favicons/meduza.ico"
    custom_styles = ["assets/css/meduza.css"]
    remove_on_xpath_match = [
        {
            "xpath": ".//*[re:match(@class, 'Banner|Spoiler|MaterialNote|RelatedRichBlock|Toolbar')]",
        },
        {
            "xpath": './/figure//*[re:match(@class, "lazy", "i")]/ancestor::figure',
        },
    ]


class ZonaMediaParser(ArticleParser):
    title_xpath = '//*[@class="mz-publish__title"]/text()'
    article_xpath = '//*[@class="mz-publish__text"]'
    timestamp_xpath = '//*[@class="mz-publish-meta__item"]/text()'
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/zona-media.html"
    favicon_path = "assets/favicons/ZonaMedia.ico"


class SvobodaParser(ArticleParser):
    title_xpath = "//head/title/text()"
    article_xpath = '//article[contains(@class, "article")]'
    article_image_xpath = None
    timestamp_xpath = None
    page_header_template = "headers/svoboda.html"
    favicon_path = "assets/favicons/svoboda.svg"
    custom_styles = ["assets/css/svoboda.css"]
    remove_on_xpath_match = [
        {
            "xpath": '//div[contains(@class, "wsw")]/following-sibling::ul',
        },
        {
            "xpath": '//div[contains(@class, "comments-parent")]',
        },
        {
            "xpath": '//div[@class="media-block-wrap"]/parent::div',
        },
        {
            "xpath": './/*[re:test(@class, "foreign-agent")]/parent::*',
        },
        {"xpath": '//div[contains(@data-owner-ct, "Article")]'},
        {
            "xpath": '//header[contains(@class, "article-hdr")]//h1',
        },
        {
            "xpath": '//div[contains(@class, "category")]',
        },
        {
            "xpath": '//div[contains(@class, "authors")]',
        },
    ]
    html_cleaner = HTMLCleaner(
        forms=False,
        frames=False,
        embedded=False,
        annoying_tags=False,
        remove_unknown_tags=False,
        safe_attrs_only=False,
    )

    def _replace_audio_content(self, content) -> None:
        audios = content.xpath(".//amp-audio")

        if audios:
            for audio in audios:
                src = audio.attrib.get("src")
                audio_tag = f'<audio src="{src}" type="audio/mpeg" name="media" controls="controls"></audio>'
                data_formatted = lxml.html.fromstring(audio_tag)
                audio.getparent().replace(audio, data_formatted)

    def _upload_images_and_replace(self, content) -> None:
        amp_images = content.xpath(".//amp-img[@src]")

        if amp_images:
            for amp_image in amp_images:
                src = amp_image.attrib.get("src")

                if src.startswith("//"):
                    src = f"https:{src}"

                if src and not src.startswith("data:"):
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"
                    amp_image.attrib.update({"src": gcs.upload_image(src)})

                    src = amp_image.attrib.get("src")
                    img_tag = f'<img src="{src}">'
                    data_formatted = lxml.html.fromstring(img_tag)
                    amp_image.getparent().replace(amp_image, data_formatted)


class VoiceOfAmericaParser(ArticleParser):
    title_xpath = '//meta[@name="title"]/@content'
    article_xpath = '//div[@class="body-container"]'
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    article_image_xpath = '//meta[@property="og:image"]/@content'
    timestamp_xpath = "//time/@datetime"
    page_header_template = "headers/voa.html"
    favicon_path = "assets/favicons/VOA.ico"
    custom_styles = ["assets/css/golosameriki.css"]
    remove_on_xpath_match = [
        {
            "xpath": '//div[contains(@class, "wsw")]/following-sibling::ul',
        },
        {
            "xpath": '//div[@id="comments"]',
        },
        {
            "xpath": '//div[@class="media-block-wrap"]/parent::div',
        },
    ]


class PaperPaperParser(ArticleParser):
    tag_title_xpath = ".//h1[contains(@class, 'text__title--slim')]|.//h1[contains(@class, 'text__title')]"
    article_xpath = ".//div[contains(@class, 'text__content')]"
    timestamp_xpath = ".//meta[@property='article:published_time']/@content"
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/paperpaper.html"
    favicon_path = "assets/favicons/paperpaper.ico"

    def get_content(self) -> str:
        """Returns the content of the article."""

        content = self.find(self.article_xpath)
        self._upload_images_and_replace(content)
        self._remove_on_xpath_match(content)
        self._upload_links_and_replace(content)
        self._replace_video_content(content)
        self._replace_audio_content(content)
        content = lxml.html.tostring(
            doc=content,
            pretty_print=True,
            encoding="unicode",
        )
        return normalize(content)


class HolodParser(ArticleParser):
    title_xpath = "//meta[@property='og:title']/@content"
    article_xpath = "//div[contains(@class, 'article__content')]"
    timestamp_xpath = "//meta[@property='article:published_time']/@content"
    article_image_xpath = "//article[contains(@class, 'article')]//img[contains(@class, 'pic-holder')]/@src"
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/holod.html"
    favicon_path = "assets/favicons/holod.ico"
    google_analytics_tracking_id = "UA-144382294-1"
    custom_styles = ["assets/css/holod.css"]
    custom_js = ["assets/js/holod/app.js"]
    remove_on_xpath_match = [
        {
            "xpath": ".//*[re:match(@class, 'm-block-inlinemore')]",
        },
        {
            "xpath": '//span[contains(@class, "texttoggle__icon")]',
        },
    ]


class BaikalJournalParser(ArticleParser):
    article_xpath = "//div[contains(@class, 'article__content')]"
    timestamp_xpath = "//meta[@property='article:published_time']/@content"
    article_image_xpath = '//meta[@property="og:image"]/@content'
    google_analytics_tracking_id = "UA-162117737-1"
    page_header_template = "headers/baikaljournal.html"
    favicon_path = "assets/favicons/baikal.ico"
    custom_styles = ["assets/css/baikaljournal.css"]
    remove_on_xpath_match = [
        {
            "xpath": '//div[contains(@class, "wp-block-wawes")]',
        },
        {"xpath": ".//*[re:match(@class, 'm-block-ctaline')]"},
    ]


class OVDInfoParser(ArticleParser):
    article_xpath = "//div[contains(@class, 'field-name-body')]|//article"
    timestamp_xpath = "//div[contains(@class, 'date-and-region')]/span/text()"
    article_image_xpath = '//meta[@property="og:image"]/@content'
    favicon_path = "assets/favicons/ovd.ico"
    page_header_template = "headers/ovdinfo.html"
    custom_styles = ["assets/css/ovd.css"]
    google_analytics_tracking_id = "G-J7DH9NKJ0R"


class GuberniaParser(ArticleParser):
    encoding = None
    image_preview_xpath = None
    title_xpath = "//h1[contains(@class, 'page_article_title')]/text()"
    article_xpath = '//div[contains(@class, "box_article_content")]'
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/gubernia-media.html"
    favicon_path = "assets/favicons/gubernia.ico"
    custom_styles = ["assets/css/gubernia.css"]
    remove_on_xpath_match = [
        {
            "xpath": "(//h3)[last()]/preceding-sibling::hr",
        },
        {
            "xpath": "(//h3)[last()]",
        },
    ]


class SemnasemParser(ArticleParser):
    article_xpath = "//div[contains(@itemprop, 'articleBody')]"
    article_image_xpath = '//meta[@property="og:image"]/@content'
    timestamp_xpath = (
        "//div[contains(@class, 'article__header-row-meta-data-date')]/text()"
    )
    page_header_template = "headers/semnasem.html"
    favicon_path = "assets/favicons/semnasem.ico"
    custom_styles = ["assets/css/semnasem.css"]
    remove_on_xpath_match = [
        {
            "xpath": ".//*[re:match(@class, 'promolink-widget')]",
        }
    ]


class VotTakParser(ArticleParser):
    article_xpath = '//div[contains(@class, "article_content")]|//div[contains (@class, "article-body")]'
    timestamp_xpath = '//span[contains(@class, "article_date")]/text()|//div[contains(@class, "date")]/text()'
    page_header_template = "headers/vot-tak.html"
    favicon_path = "assets/favicons/vot-tak.ico"
    image_preview_xpath = None
    article_image_xpath = '//meta[@property="og:image"]/@content'
    custom_styles = ["assets/css/vot-tak.css"]

    def _upload_images_and_replace(self, content) -> None:
        images = content.xpath("//amp-img[@src]") or []

        for image in images:
            src = image.attrib.get("src")
            img_tag = f'<img src="{src}">'
            data_formatted = lxml.html.fromstring(img_tag)
            image.getparent().replace(image, data_formatted)

            if src and not src.startswith("data:"):
                if src.startswith("/"):
                    src = f"https://{self.page.hostname()}{src}"
                image.attrib.update({"src": gcs.upload_image(src)})


class NewTimesParser(ArticleParser):
    article_xpath = '//div[@id="full"]'
    article_image_xpath = '//meta[@property="og:image"]/@content'


class AvtonomParser(ArticleParser):
    article_xpath = '//*[contains(@class, "field-name-body")]'
    timestamp_xpath = '//meta[@property="og:updated_time"]/@content'
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/avtonom.html"
    favicon_path = "assets/favicons/avtonom.ico"
    custom_styles = ["assets/css/avtonom.css"]


class DoxaParser(ArticleParser):
    title_xpath = "//h1[contains(@class, 'newsitem-title exo')]/text()"
    article_xpath = "//div[contains(@class, 'text-white newsitem-text merri')]"
    timestamp_xpath = "//meta[@property='article:published_time']/@content"
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/doxa.html"
    favicon_path = "assets/favicons/doxa.ico"
    custom_styles = ["assets/css/doxa.css"]


class RepublicParser(ArticleParser):
    article_xpath = '//div[contains(@class, "cms-content post-content js-post-content js-mediator-article")]'
    image_preview_xpath = None
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/republic.html"
    favicon_path = "assets/favicons/republic.ico"


class ItsMyCityParser(ArticleParser):
    article_xpath = '//div[contains(@id, "dev_page_content")]'
    timestamp_xpath = "//meta[@property='article:published_time']/@content"
    image_preview_xpath = None
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/itsmycity.html"
    favicon_path = "assets/favicons/ITC.ico"
    custom_styles = ["assets/css/itsmycity.css"]

    def _upload_images_and_replace(self, content) -> None:
        images = content.xpath(".//img[@src]|.//img[@data-src]") or []

        ignored_attributes = (
            "height",
            "id",
            "sizes",
            "srcset",
            "styles",
            "width",
        )

        for image in images:
            src = image.attrib.get("src")

            if src == "https://itsmycity.ru/imgloader.png":
                data_src = image.attrib.get("data-src")
                src = data_src

            if not src:
                data_src = image.attrib.get("data-src")
                if data_src:
                    src = data_src

            for attr in ignored_attributes:
                image.attrib.pop(attr, None)

            # If the image is base64 encoded, we don't need to upload it.
            if src and not src.startswith("data:"):
                # If src is relative, we need to make it absolute.
                if src.startswith("/"):
                    src = f"https://{self.page.hostname()}{src}"
                # Upload the image and replace the src attribute.
                image.attrib.update({"src": gcs.upload_image(src)})


class BBCRussianParser(ArticleParser):
    article_xpath = '//main[contains(@role, "main")]'
    timestamp_xpath = "//meta[@name='article:published_time']/@content"
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    article_image_xpath = None
    page_header_template = "headers/bbc.html"
    favicon_path = "assets/favicons/bbcrussian.ico"
    custom_styles = ["assets/fonts/bbc/fonts.css", "assets/css/bbc.css"]
    remove_on_xpath_match = [
        {
            "xpath": '//div[contains(@class, "bbc-1151pbn ebmt73l0")]',
        },
        {
            "xpath": '//div[contains(@class, "e1j2237y7 bbc-q4ibpr ebmt73l0")]',
        },
        {
            "xpath": '//div[contains(@class, "bbc-1151pbn ebmt73l0")]',
        },
        {
            "xpath": '//div[contains(@class, "bbc-zvnee0 e1rfboeq6")]',
        },
        {
            "xpath": '//div[contains(@class, "e1wxwc760 bbc-1bh6w8m ebmt73l0")]',
        },
    ]

    def _upload_images_and_replace(self, content) -> None:
        figures = content.xpath(".//div[contains(@class, 'bbc-1ka88fa ebmt73l0')]")

        for figure in figures:
            images = figure.xpath(".//img[@src]") or []

            for image in images:
                src = image.attrib.get("src")
                img_tag = f'<img src="{src}" class="bbc_img">'
                data_formatted = lxml.html.fromstring(img_tag)
                figure.getparent().replace(figure, data_formatted)

                if src and not src.startswith("data:"):
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"
                    figure.attrib.update({"src": gcs.upload_image(src)})


class SkatMediaParser(ArticleParser):
    encoding = None
    article_xpath = '//div[contains(@class, "content_article")]'
    timestamp_xpath = '//time[contains(@class, "date")]/text()'
    page_header_template = "headers/skat.html"
    favicon_path = "assets/favicons/skat.ico"
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    article_image_xpath = '//meta[@property="og:image"]/@content'
    custom_styles = ["assets/css/skat.css"]
    remove_on_xpath_match = [
        {
            "xpath": '//h1[contains(@class, "title")]',
        },
        {
            "xpath": '//time[contains(@class, "date")]',
        },
        {
            "xpath": '//div[contains(@class, "meta")]',
        },
        {
            "xpath": '//div[contains(@class, "support_container")]',
        },
        {
            "xpath": '//div[contains(@class, "share_container")]',
        },
        {
            "xpath": '//img[contains(@class, "load_container")]',
        },
        {
            "xpath": '//div[contains(@class, "client-only-placeholder")]',
        },
    ]


class ProektMediaParser(ArticleParser):
    article_xpath = '//div[contains(@class, "single-post__article js-post-article")]'
    timestamp_xpath = ".//meta[@property='article:published_time']/@content"
    article_image_xpath = '//meta[@property="og:image"]/@content'


class ZasekinParser(ArticleParser):
    article_xpath = '//article[contains(@class, "article-body")]'
    timestamp_xpath = ".//meta[@property='article:published_time']/@content"
    article_image_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/zasekin.html"
    favicon_path = "assets/favicons/zasekin.ico"
    custom_styles = ["assets/fonts/zasekin/fonts.css", "assets/css/zasekin.css"]


class ChertaMediaParser(ArticleParser):
    timestamp_xpath = None
    article_image_xpath = None
    article_xpath = '//div[contains(@class, "single-story")]'
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/cherta.html"
    favicon_path = "assets/favicons/cherta.ico"
    custom_styles = ["assets/css/cherta.css"]
    remove_on_xpath_match = [
        {
            "xpath": "//div[contains(@class, 'single-page__footer-info')]",
        },
    ]


class ProtokolBandParser(ArticleParser):
    article_xpath = '//div/div[contains(@class, "td_block_wrap tdb_single_content tdi_69 td-pb-border-top td_block_template_2 td-post-content tagdiv-type")]'
    timestamp_xpath = '//meta[@property="article:published_time"]/@content'
    article_image_xpath = (
        '//div[contains(@class, "tdb-block-inner td-fix-index")]//a/img'
    )
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/protokolband.html"
    favicon_path = "assets/favicons/protokol-favicon.ico"
    custom_styles = ["assets/css/protokolband.css"]
    remove_on_xpath_match = [
        {
            "xpath": "//style",
        },
    ]

    def _upload_images_and_replace(self, content) -> None:
        tags_a = content.xpath("//figure[contains(@class, 'wp-caption aligncenter')]/a")

        for tag_a in tags_a:
            src = tag_a.attrib.get("href")

            if src and not src.startswith("data:"):
                if src.startswith("/"):
                    src = f"https://{self.page.hostname()}{src}"
                tag_a.attrib.update({"src": gcs.upload_image(src)})

                img_tag = f'<img src="{src}" class="protokol_img">'
                data_formatted = lxml.html.fromstring(img_tag)
                tag_a.getparent().replace(tag_a, data_formatted)

    def get_article_image(self) -> t.Union[str, None]:
        try:
            content = self.find(self.article_xpath)
            styles = content.xpath(self.article_image_xpath)
            if styles:
                for style in styles:
                    div_style = style.attrib.get("src")
                    return div_style
        except:
            return None


class TeStParser(ArticleParser):
    article_xpath = '//div[contains(@class, "tst-entry-container")]|//div[contains(@class, "col-main entry-content")]'
    title_xpath = '//h1[contains(@class, "tst-entry-title")]/text()|//h1[contains(@class, "entry-title")]/text()'
    timestamp_xpath = '//meta[@property="article:published_time"]/@content'
    article_image_xpath = "//img[contains(@class, 'attachment-post-thumbnail-medium size-post-thumbnail-medium wp-post-image')]/@src|//div[contains(@class, 'tpl-post-full__intro')]/img/@src"
    image_preview_xpath = '//meta[@property="og:image"]/@content'
    page_header_template = "headers/te_st.html"
    favicon_path = "assets/favicons/te_st_favicon.ico"
    google_analytics_tracking_id = "UA-511139-48"
    custom_styles = ["assets/css/te_st.css"]
    remove_on_xpath_match = [
        {
            "xpath": './/footer[contains(@class, "entry-footer")]',
        },
    ]

    def _upload_images_and_replace(self, content) -> None:
        images = content.xpath(".//img[@src]|.//img[@data-src]") or []
        ignored_attributes = (
            "height",
            "id",
            "sizes",
            "srcset",
            "styles",
            "width",
        )

        for image in images:
            src = image.attrib.get("src")

            if src.startswith("//"):
                src = f"https:{src}"

            if not src:
                data_src = image.attrib.get("data-src")
                if data_src:
                    src = data_src

            for attr in ignored_attributes:
                image.attrib.pop(attr, None)

            # If the image is base64 encoded, we don't need to upload it.
            if src and not src.startswith("data:"):
                # If src is relative, we need to make it absolute.
                if src.startswith("/"):
                    src = f"https://{self.page.hostname()}{src}"
                # Upload the image and replace the src attribute.
                image.attrib.update({"src": gcs.upload_image(src)})


class AzadliqParser(SvobodaParser):
    page_header_template = "headers/azadliq.html"


class AzerAzadliqParser(AzadliqParser):
    page_header_template = "headers/azerazadliq.html"
    page_footer_template = "footer/azerazadliq_footer.html"


class TurkAzadliqParser(AzadliqParser):
    page_header_template = "headers/turkazadliq.html"
    page_footer_template = "footer/turkazadliq_footer.html"


class RadiFardaParser(SvobodaParser):
    page_header_template = "headers/radiofarda.html"
    page_footer_template = None
    custom_styles = ["assets/css/svoboda.css", "assets/css/radiofarda.css"]


class KavkazRealParser(SvobodaParser):
    page_header_template = "headers/kavkazreal.html"


class MeduzaAmpParser(ArticleParser):
    need_the_url = None
    tag_title_xpath = '//h1[@class="style__Root-sc-7350mp-0 cTMjtX"]'
    article_xpath = '//div[@data-original-class="Material"]'
    timestamp_xpath = "//time/amp-date-display/div/div/text()"
    page_header_template = "headers/meduza.html"
    favicon_path = "assets/favicons/meduza.ico"
    custom_styles = ["assets/css/meduza.css"]
    remove_on_xpath_match = [
        {
            "xpath": '//div[contains(@class, "bdJWSX")]',
        },
        {
            "xpath": '//label[contains(@data-original-class, "Button")]',
        },
        {
            "xpath": '//div[contains(@data-original-class, "Root")]/input[contains(@type, "checkbox")]',
        },
    ]

    def _upload_images_and_replace(self, content) -> None:
        images = content.xpath(".//img[@src]|.//img[@data-src]")
        amp_images = content.xpath(".//amp-img/amp-img[@src]|.//amp-img[@src]")

        ignored_attributes = (
            "height",
            "id",
            "sizes",
            "srcset",
            "styles",
            "width",
        )

        # TODO: Refactor this mess!
        # TODO: Remove duplicated code!

        if amp_images:
            for amp_image in amp_images:
                src = amp_image.attrib.get("src")

                if src.startswith("//"):
                    src = f"https:{src}"

                for attr in ignored_attributes:
                    amp_image.attrib.pop(attr, None)

                # If the image is base64 encoded, we don't need to upload it.
                if src and not src.startswith("data:"):
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"
                    amp_image.attrib.update({"src": gcs.upload_image(src)})

                    src = amp_image.attrib.get("src")
                    img_tag = f'<img src="{src}">'
                    data_formatted = lxml.html.fromstring(img_tag)
                    amp_image.getparent().replace(amp_image, data_formatted)

        if images:
            for image in images:
                src = image.attrib.get("src")

                if src.startswith("//"):
                    src = f"https:{src}"

                if not src:
                    data_src = image.attrib.get("data-src")
                    if data_src:
                        src = data_src

                for attr in ignored_attributes:
                    image.attrib.pop(attr, None)

                # If the image is base64 encoded, we don't need to upload it.
                if src and not src.startswith("data:"):
                    # If src is relative, we need to make it absolute.
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"
                    # Upload the image and replace the src attribute.
                    image.attrib.update({"src": gcs.upload_image(src)})


class MeduzaLightParser(ArticleParser):
    tag_title_xpath = ""
    title_xpath = None
    need_the_url = None
    timestamp_xpath = None
    article_image_xpath = None
    styles_link = '//head/link[contains(@rel, "stylesheet")]'
    article_xpath = "//main"
    favicon_path = "assets/favicons/meduza.ico"


class EchofmParser(ArticleParser):
    timestamp_xpath = None
    article_image_xpath = None
    article_xpath = '//article[contains(@class, "wp-embed-responsive")]'
    page_header_template = "headers/echofm.html"
    favicon_path = "assets/favicons/echofm_favicon.ico"
    custom_styles = ["assets/css/echofm.css"]

    # remove_on_xpath_match = [
    #     {
    #         "xpath": '//div[contains(@class, "ieaoln")]',
    #     }
    # ]

    def _upload_images_and_replace(self, content) -> None:
        figures = content.xpath(".//span/noscript")

        ignored_attributes = (
            "height",
            "id",
            "sizes",
            "srcset",
            "styles",
            "width",
            "srcset",
        )

        if figures:
            for figure in figures:
                image = figure[0]

                src = image.attrib.get("src")

                for attr in ignored_attributes:
                    image.attrib.pop(attr, None)

                if src and not src.startswith("data:"):
                    if src.startswith("/"):
                        src = f"https://{self.page.hostname()}{src}"

                    figure.attrib.update({"src": gcs.upload_image(src)})
                    img_tag = f'<img src="{gcs.upload_image(src)}">'
                    data_formatted = lxml.html.fromstring(img_tag)
                    figure.getparent().replace(figure, data_formatted)


PARSERS = {
   # "newtimes.ru": NewTimesParser,
    "meduza.io": MeduzaAmpParser,
    "website-light.preview.meduza.io": MeduzaLightParser,
   ### "zona.media": ZonaMediaParser,
   # "www.svoboda.org": SvobodaParser,
   # "www.golosameriki.com": VoiceOfAmericaParser,
   ### "paperpaper.ru": PaperPaperParser,
   ### "paperpaper.io": PaperPaperParser,
    "holod.media": HolodParser,
    ### "ovdinfo.org": OVDInfoParser,
   ###  "ovd.news": OVDInfoParser,
   # "data.ovdinfo.org": OVDInfoParser,
   ###  "baikal-journal.ru": BaikalJournalParser,
   ###  "gubernia.media": GuberniaParser,
   ###  "semnasem.org": SemnasemParser,
   # "vot-tak.tv": VotTakParser,
   ###  "avtonom.org": AvtonomParser,
   # "news.doxajournal.ru": DoxaParser,
   # "republic.ru": RepublicParser,
   ###  "itsmycity.ru": ItsMyCityParser,
    "www.bbc.com": BBCRussianParser,
   # "bbc.in": BBCRussianParser,
    "skat.media": SkatMediaParser,
   # "www.proekt.media": ProektMediaParser,
   # "zasekin.ru": ZasekinParser,
   ###  "cherta.media": ChertaMediaParser,
   ###  "protokol.band": ProtokolBandParser,
   # "te-st.ru": TeStParser,
   # "te-st.org": TeStParser,
   # "rus.azathabar.com": AzadliqParser,
   # "www.azadliq.org": AzerAzadliqParser,
   # "www.azathabar.com": TurkAzadliqParser,
   # "www.kavkazr.com": KavkazRealParser,
   # "www.radiofarda.com": RadiFardaParser,
   ###  "echofm.online": EchofmParser,
}


async def create_parser_object(url) -> Parser:
    url = url.strip()
    # user_agent = {'user-agent': 'Googlebot/2.1 (+http://www.google.com/bot.html)'}
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            text_content = await response.text()
            webpage = WebPage(url, text_content)

            hostname = webpage.hostname()
            parser_class = PARSERS.get(hostname)
            if parser_class is None:
                raise UnsupportedWebsite(f"Unknown hostname: {hostname}")
            return parser_class(webpage)
