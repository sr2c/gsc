import hashlib
import time
from io import BytesIO
from urllib.parse import urlparse

import requests
from bs4 import BeautifulSoup
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic import BaseModel, HttpUrl
from starlette.status import HTTP_401_UNAUTHORIZED

from app.core.config import settings
from app.core.storage import CloudStorage
from app.parser.utils import normalize
from urllib.parse import urlparse

router = APIRouter()
security = HTTPBasic()

bucket = CloudStorage(
    settings.GOOGLE_CLOUD_STORAGE_BUCKET,
    settings.GOOGLE_CONFIG_PATH,
)

API_CREDS = { settings.HTTP_BASIC_USERNAME: settings.HTTP_BASIC_PASSWORD}


with open("page_credentials.json", "r") as f:
    credentials = json.load(f)


def get_current_user(credentials: HTTPBasicCredentials = Depends(security)):
    username = credentials.username
    password = credentials.password
    if API_CREDS.get(username) == password:
         return username
    raise HTTPException(status_code=403, detail="Access Forbidden")


class Page(BaseModel):
    url: HttpUrl
    timestamped: bool = True
    def domain(self) ->str:
        return urlparse(url).netloc

    def sha1(self) -> str:
        return hashlib.sha1(self.url.encode("utf-8")).hexdigest()[:8]

    def parsable_url(self) -> str:
        return self.url.replace(
            "//meduza.io",
            "//website-light.preview.meduza.io",
        )

    def build_absolute_url(self, path: str) -> str:
        url = urlparse(self.parsable_url())
        return f"https://{url.hostname}{path}"

    def finale_filename(self) -> str:
        return f"{self.sha1()}.html"


def upload_image_to_bucket(image_url: str):
    bucket.upload_image(image_url)


@router.post("/copy/meduza/", include_in_schema=False)
@router.post("/conserve/", include_in_schema=False)
async def conserve_page(current_user: str = Depends(get_current_user), page: Page)
    try:
        page_creds = credentials.get(page.domain, {})
        response = requests.get(page.parsable_url(), auth=HTTPBasicAuth(page_creds.get("username"), page_creds.get("password")
        soup = BeautifulSoup(response.text, "html.parser")
        soup.head.append(soup.new_tag("style", type="text/css"))

        for link in soup.find_all("link"):
            rel = link.get("rel", [])

            if "stylesheet" in rel:
                path = link.get("href")
                absurl = page.build_absolute_url(path)
                cssfile_response = requests.get(absurl)
                soup.head.style.append(cssfile_response.text.strip())

            link.decompose()

        for img in soup.find_all("img"):
            path = img.get("src")
            img["src"] = bucket.predict_image_public_url(path)
            background_tasks.add_task(upload_image_to_bucket, path)

        public_url = bucket.upload_file(
            file_name=page.finale_filename(),
            file_object=BytesIO(normalize(str(soup)).encode("utf-8")),
            content_type="text/html",
        )

        timestamp = int(time.time())

        if page.timestamped:
            public_url += f"?v={timestamp}"

        return JSONResponse(
            content={
                "url": public_url,
                "meta": {
                    "timestamp": timestamp,
                    "source_url": page.url,
                    "source_url_sha1": page.sha1(),
                },
            },
            status_code=status.HTTP_201_CREATED,
        )
    except Exception as _e:
        return JSONResponse(
            {"error": "unknown error"},
            status_code=status.HTTP_400_BAD_REQUEST,
        )
