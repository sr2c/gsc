import hashlib
import io
import mimetypes
import typing
from urllib.parse import urlparse

import requests
from google.cloud.storage import Client

from app.core.config import settings

__all__ = ["gcs", "CloudStorage"]


class CloudStorage:
    def __init__(self, bucket_name: str, json_credentials_path: str) -> None:
        self.json_credentials_path = json_credentials_path
        self.client = Client.from_service_account_json(
            json_credentials_path=self.json_credentials_path
        )
        self.bucket_name = bucket_name
        self.bucket = self.client.get_bucket(self.bucket_name)

    def upload_file(
        self,
        file_name: str,
        file_object: typing.IO,
        content_type: typing.Optional[str] = None,
    ) -> str:
        blob = self.bucket.blob(file_name)
        blob.upload_from_file(file_object, content_type=content_type)
        return blob.public_url

    def upload_page(self, file_name: str, file_object: typing.IO) -> str:
        blob = self.bucket.blob(file_name)
        blob.upload_from_file(file_object, content_type="text/html")
        return blob.public_url

    def predict_image_public_url(self, url: str) -> str:
        filename, extension, host, sha1 = self._deconstruct_url(url)
        return (
            f"https://storage.googleapis.com/"
            f"{self.bucket_name}/"
            f"images/{host}/{sha1}.{extension}"
        )

    def _deconstruct_url(self, url: str):
        filename = url.split("/")[-1]
        extension = filename.split(".")[-1]
        host = urlparse(url).hostname
        sha1 = hashlib.sha1(url.encode("utf-8")).hexdigest()
        return filename, extension, host, sha1

    def upload_image(self, url: str) -> str:
        filename, extension, host, sha1 = self._deconstruct_url(url)

        with requests.get(url, stream=True) as response:
            try:
                content_type = mimetypes.guess_type(filename)[0]
            except IndexError:
                content_type = None

            print(content_type, extension, url)

            return self.upload_file(
                file_name=f"images/{host}/{sha1}.{extension}",
                file_object=io.BytesIO(response.content),
                content_type=content_type,
            )


gcs = CloudStorage(
    settings.GOOGLE_CLOUD_STORAGE_BUCKET,
    settings.GOOGLE_CONFIG_PATH,
)
